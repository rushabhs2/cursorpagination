from django.apps import AppConfig


class CursorpagnConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cursorpagn'
