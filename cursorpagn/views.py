from django.shortcuts import render

from .serializers import StudentSerializer
from .models import Students
from rest_framework.generics import ListAPIView
from cursorpagn.mypagination import MyCursorPagination


# Create your views here.

class StudentList(ListAPIView):
    queryset = Students.objects.all()
    serializer_class = StudentSerializer
    pagination_class = MyCursorPagination
